////////////PARAMETERS////////////
p_width = 18;
p_depth = 18;
p_height = 170;
p_wall_thikness = 0.8;
p_zip_tei_width = 2.1;
p_zip_tei_height = 1;
//////////////////////////////////

$fn = 100;
v_outter_width = p_width + p_wall_thikness*2;

base_3d(p_width, p_depth, p_wall_thikness, p_height);
translate([0,0,10]) tei_holder_3d_all();
translate([0,0,p_height-10]) tei_holder_3d_all();



module base_2d(width, depth) {
	difference() {
		union() {
			circle(d = width);
			translate([-width/2,0,0]) square([width, depth-width/2]);
		}	
		translate([-width/2,depth-width/2,0]) square([width, width-depth]);
	}
}

module base_2d_cutted(width, depth, wall_thikness){
	difference() {
		base_2d(width+(wall_thikness*2), depth);
		base_2d(width, depth);
	}
}

module base_3d(width, depth, wall_thikness, height){
	linear_extrude(height)
		base_2d_cutted(width, depth, wall_thikness);
}

module tei_cat_3d(width, wall_thikness){
	coors = angle_to_coors(width/2+wall_thikness/2, -25);
	cut_a = 90 - two_points_angle(coors, [0,-width/2]);
	translate([coors[0],coors[1],0])
	rotate(cut_a) 
		cube([wall_thikness+width/4, 1, 2], true);	
}

module tei_cats_3d(width, wall_thikness){
	tei_cat_3d(width, wall_thikness);
	mirror([1,0,0]) tei_cat_3d(width, wall_thikness);
}

module tei_holder_2d(zip_w, zip_h){
	a = [
		[zip_w/2, 0],
		[5, 0],
		[zip_w/2, zip_h],
		[zip_w/2, 0],
	];
	polygon(a);
	mirror([1,0,0]) polygon(a);
	//translate([0,-p_depth/4,0]) square([10, p_depth/2], true);
}

module tei_holder_3d_side(depth){
	rotate(270, [0, 0, 1])
	rotate(90, [0, 1, 0])
	translate([0,0,-depth])
	linear_extrude(depth/2)
		tei_holder_2d(p_zip_tei_width,p_zip_tei_height);
}

module tei_holder_3d_round(width){
	difference() {
		rotate_extrude()
		translate([width/2,0,0]) rotate(270, [0, 0, 1]) tei_holder_2d(p_zip_tei_width,p_zip_tei_height);
		translate([0,width/2,0])cube([width*2, width, 3+20], true);
	}
}

module tei_holder_3d_all(){
	tei_holder_3d_round(v_outter_width);
translate([p_width/2 + p_wall_thikness,-p_depth/2 + p_wall_thikness,0]) tei_holder_3d_side(p_depth - p_wall_thikness*2);
mirror([1,0,0]) translate([p_width/2 + p_wall_thikness,-p_depth/2 + p_wall_thikness,0]) tei_holder_3d_side(p_depth - p_wall_thikness*2);
}

function angle_to_coors(r, a) = [r*cos(a), r*sin(a)];
function two_points_angle(p1, p2) = atan2(p1[0]-p2[0], p1[1]-p2[1]);