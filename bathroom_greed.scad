$fn = 100;

////////////PARAMETERS////////////
outer_diameter = 75.3;
inner_diameter = 64;
height_ = 2.7;
row_number = 4;
column_number = 3;
grid_wall_thickness = 6;
corner_radius = 3;
//////////////////////////////////

linear_extrude(height = height_){
	offset(r = -corner_radius) offset(r = corner_radius)
	union() {
		ring_2d(outer_diameter, inner_diameter);
		difference(){
			translate([0,0,0]) 
				grid_2d(
					inner_diameter, 
					inner_diameter,
					column_number,	
					row_number,  
					grid_wall_thickness
				);
			ring_2d(200,outer_diameter);
		}
	}
}

module grid_2d(w, d, wn, dn, wt){
	step_w = w/wn;
	step_d = d/dn;
	for(i = [-w/2 : step_w : w/2+0.1]){
		translate([i,0,0]) square([wt,w],true);
	}
	for(i = [-d/2 : step_d : d/2+0.1]){
		translate([0,i,0]) square([d,wt],true);
	}
}
module ring_2d(d1, d2){
	difference(){
		circle(d=d1);
		circle(d=d2);
	}
}