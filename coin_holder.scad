////////////PARAMETERS////////////
wall_thickness = 1.8;
floor_thickness = 0.8;
stack_height = 40;
base_height = 13;
base_depth = 60;
base_width = 120; //will be shorter becauce of cut
radii = [23/2, 26/2, 24/2, 21.5/2]; //Russian Rubles
///////////////////////////////////

$fn = 200;
c_small_r_coef = 0.833;
c_stack_a = 270;
v_wt = wall_thickness;
v_ft = floor_thickness;
v_stack_h = stack_height;
v_base_h = base_height;
v_base_d = base_depth;
v_base_w = base_width;
v_radii = radii;
v_big_r = (pow(v_base_w/2, 2) + pow(v_base_d/2, 2))/v_base_d;
v_big_r_lift = v_big_r - v_base_d/2;
v_small_r = v_base_d*c_small_r_coef;
v_small_r_lift = (v_base_d - v_small_r)*2;
v_intersection_point	= circles_intersection(v_big_r, 0, -v_big_r_lift, v_small_r, 0, v_small_r_lift);
v_start_point = [v_intersection_point[1][0], v_intersection_point[1][1] + v_big_r_lift];
v_start_lenght = angle_to_length(v_big_r, coors_to_angle(v_start_point)) - v_wt;

union(){
	stacks_3d();
	difference(){
		base_3d();
		cuts_3d();
	}
}

module base_3d(){
	difference() {
		linear_extrude(v_base_h){
			 base_2d();
		}
		translate([0,0,v_ft]) 
		linear_extrude(v_base_h){
			 offset(delta = -v_wt) base_2d();
		}
	}
}

module base_2d(){
	difference() {
		translate([0,v_small_r_lift,0]) circle(r=v_small_r);
		difference() {
			translate([0,v_big_r_lift,0]) circle(r=v_big_r*2);
			translate([0,-v_big_r_lift,0]) circle(r=v_big_r);
		}
	}
	difference() {
		intersection() {
			translate([0,v_big_r_lift,0]) circle(r=v_big_r);
			translate([0,-v_big_r_lift,0]) circle(r=v_big_r);
		}
		translate([-v_big_r/2,0,0]) square([v_big_r, v_big_r*2], true);
	}
}

module stack_3d(r) {
	linear_extrude(v_stack_h){
		rotate(-45)
		difference() {
			pie_slice(r + v_wt, c_stack_a);
			pie_slice(r, c_stack_a);
		}
	}
	cylinder(h = v_ft, r = r, center = false);
}

module stacks_3d(){
	for(i = [0:len(radii)-1]){
		length_offset = v_start_lenght - arr_sum(v_radii, 0, i)*2 - v_radii[i] - v_wt*i*1.1;
		point = length_to_coors(v_big_r, length_offset);
		angle = coors_to_angle(point) - 90;
		translate([point[0], point[1] - v_big_r_lift]) rotate(angle) stack_3d(radii[i]);
	}
}

module cuts_3d(){
	for(i = [0:len(v_radii)-1]){
		length_offset = v_start_lenght - arr_sum(v_radii, 0, i)*2 - v_radii[i] - v_wt*i*1.1;
		point = length_to_coors(v_big_r, length_offset);
		translate([point[0], point[1] - v_big_r_lift, v_ft]) cylinder(h=v_base_h+1, r=v_radii[i] + v_wt);
	}
}

module pie_slice(r=3.0,a=30) {
	rr = r;
	union() {
		if(a<=90){
			intersection() {
				circle(r=rr);
				square(rr);
				rotate(a-90) square(rr);
			}
		}
		if(a<=180&&a>90){
			pie_slice(rr, 90);
			rotate(90) pie_slice(rr, a - 90);
		}
		if(a<=270&&a>180){
			pie_slice(rr, 180);
			rotate(180) pie_slice(rr, a - 180);
		}
		if(a<=360&&a>270){
			pie_slice(rr, 270);
			rotate(270) pie_slice(rr, a - 270);
		}
	}
}

function angle_to_coors(r, a) = [r*cos(a), r*sin(a)];
function coors_to_angle(c) = atan2(c[1], c[0]);
function angle_to_length(r, a) = ((a)/360) * 2*PI*r;
function length_to_angle(r, l) = (l*360)/(2*PI*r);
function length_to_coors(r, l) = angle_to_coors(r, length_to_angle(r, l));
function arr_sum(arr, from, to, sum = 0) = from < to ? arr_sum(arr, from + 1, to, sum + arr[from]) : sum;

//https://stackoverflow.com/questions/3349125/circle-circle-intersection-points
function circles_intersection(r0,x0,y0,r1,x1,y1) = let(
	d=sqrt(pow((x1-x0),2) + pow((y1-y0),2)),
	a=(pow(r0,2)-pow(r1,2)+pow(d,2))/(2*d),
	h=sqrt(pow(r0,2)-pow(a,2)),
	xx=(x1-x0)*(a/d)+x0,
	yy=(y1-y0)*(a/d)+y0,
	x2=xx+h*(y1-y0)/d,
	y2=yy-h*(x1-x0)/d,   
	x3=xx-h*(y1-y0)/d,
	y3=yy+h*(x1-x0)/d
) [[x2,y2],[x3,y3]];
