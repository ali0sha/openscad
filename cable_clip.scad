////////////PARAMETERS////////////
main_radius = 10;
ear_radius = 5;
wall_thickness = 1.6;
height = 1.6;
angle = 20;
///////////////////////////////////

$fn = 200;
v_height = height;
v_cut_angle = 90 - angle;
v_wall_thickness = wall_thickness;
v_main_inner_r = main_radius;
v_main_outter_r = main_radius + wall_thickness;
v_ear_outter_r = ear_radius;
v_ear_inner_r = ear_radius - wall_thickness;
v_ear_coors = coors_on_circle(v_main_inner_r + ear_radius, v_cut_angle);

union() {
	main_half();
	mirror([1, 0, 0]) main_half();
}

module main_half(){
	linear_extrude(v_height){
		rotate(-90)
		difference() {
			pie_slice(v_main_outter_r,90+v_cut_angle);
			pie_slice(v_main_inner_r,90+v_cut_angle);
		}
	}
	linear_extrude(v_height){
		translate([v_ear_coors[0], v_ear_coors[1],0]) ear();
	}
}

module ear(){
	a = 180;
	dot_coors = coors_on_circle(v_ear_inner_r, v_cut_angle);
	
	union() {
		rotate(v_cut_angle) difference() {
			pie_slice(v_ear_outter_r,a);
			pie_slice(v_ear_inner_r,a);
		}
		translate([dot_coors[0], dot_coors[1], 0]) circle(v_wall_thickness);
	}
}

module pie_slice(r=3.0,a=30) {
	rr = r;
	union() {
		if(a<=90){
			intersection() {
				circle(r=rr);
				square(rr);
				rotate(a-90) square(rr);
			}
		}
		if(a<=180&&a>90){
			pie_slice(rr, 90);
			rotate(90) pie_slice(rr, a - 90);
		}
		if(a<=270&&a>180){
			pie_slice(rr, 180);
			rotate(180) pie_slice(rr, a - 180);
		}
		if(a<=360&&a>270){
			pie_slice(rr, 270);
			rotate(270) pie_slice(rr, a - 270);
		}
	}
}

function coors_on_circle(r, a) = [r*cos(a), r*sin(a)];