//aseklenkov

////////////PARAMETERS////////////
diameters = [5, 4, 3.5, 3]; //from higher to lower
clips_count = 10; 
clips_offset = 1.5;
wall_thickness = 1.5;
height = 6;
base_width = 3;
mirrored = true;
//////////////////////////////////

function arr_sum(arr, from, to, sum = 0) = from < to ? arr_sum(arr, from + 1, to, sum + arr[from]) : sum;

module cut2D(d){
	translate([d/20,0,0]) translate([d/2,d/2,0]) circle(d = d, $fs = 0.5);
	translate([-1,d/6,0]) square([d - d/5,d/1.5]);
}

module cuts2D(diameters){
	for(i = [0:len(diameters)-1]){
		y_offset = (max(diameters) - diameters[i])/2;
		x_offset = arr_sum(diameters, 0, i) + arr_sum(diameters, 0, i)*0.02;
		translate([x_offset,y_offset,0]) cut2D(diameters[i]);
	}
}

module clip2D(diameters){
	r_offset = (wall_thickness<2) ? wall_thickness/3.6 : 0.5 ;
	offset(r = r_offset)
	offset(delta = -r_offset)
	difference() {
			translate([0,wall_thickness,0]) offset(delta = wall_thickness) cuts2D(diameters);
			translate([0,wall_thickness,0]) cuts2D(diameters);
			translate([-wall_thickness-1,0,0]) square([wall_thickness+1,max(diameters) + wall_thickness*2]);
	}
}

module clip3D(diameters, height){
	linear_extrude(height = height, center = false, convexity = 10, twist = 0){
		clip2D(diameters);
		}
	}

module clips3D(diameters, height, clips_count,  clip_depth, clips_offset){
	for(i = [0:clips_count-1]){
		translate([0,(clip_depth+clips_offset)*i,0]) clip3D(diameters, height);
	}
}

module base(width, depth, height){
	r = width*0.4;
	linear_extrude(height = height, center = false, convexity = 10, twist = 0, $fn = 16){
		translate([r,r,0]) offset(r = r) square([width - 2*r, depth - 2*r]);
	}
}

clip_depth = max(diameters) + 2*wall_thickness;
base_x_offset = arr_sum(diameters, 0, len(diameters))*1.04;
base_y_offset = (max(diameters) - diameters[len(diameters)-1])/2 + wall_thickness;
base_depth = clips_count*clip_depth + (clips_count - 1)*clips_offset;

$fn = 50;
union() {
	clips3D(diameters, height, clips_count,  clip_depth, clips_offset);
	if(mirrored){
		translate([base_x_offset*2 + base_width,0,0]) mirror([1,0,0]) 
			clips3D(diameters, height, clips_count,  clip_depth, clips_offset);
	}
	translate([base_x_offset, 0, 0]) base(base_width,base_depth,height);
}
