//aseklenkov
//remix of https://www.thingiverse.com/thing:2562937

module main_box(width, depth, height, wall, floor_) {
	difference() {
		cube([width,depth,height],0);
		translate([wall,wall,floor_]) cube([width-wall*2,depth-wall*2,height+1-floor_],0);
	}
	translate([1,		1,		0])	cylinder(height,3,3,0);
	translate([1,		depth-1,0])	cylinder(height,3,3,0);
	translate([width-1,	1,		0])	cylinder(height,3,3,0);
	translate([width-1,	depth-1,0])	cylinder(height,3,3,0);
}
module cuts(width, depth, height) {
	translate([1,		1,		-1])	cylinder(7,2,2,0);
	translate([1,		depth-1,-1])	cylinder(7,2,2,0);
	translate([width-1,	1,		-1])	cylinder(7,2,2,0);
	translate([width-1,	depth-1,-1])	cylinder(7,2,2,0);
}

module pin(){
	union(){
		cylinder(4.5,1.5,1.5,0);
		translate([0,0,4.5]) sphere(1.5);
	}
}

module pins(width, depth, height){
	if(height > 6) {
		width = width -1;
		depth = depth -1;
		height = height -1;
		union(){
			translate([1,		1,		height])	pin();
			translate([1,		depth,	height])	pin();
			translate([width,	1,		height])	pin();
			translate([width,	depth,	height])	pin();
		}
	}
}

module wall(width, depth, height, wall_thickness, x_1, y_1, x_2, y_2){
	x_1 = width*x_1/100;
	y_1 = depth*y_1/100;
	x_2 = width*x_2/100;
	y_2 = depth*y_2/100;
	if(x_1 > x_2){
		x_1 = x_1 + x_2;
		x_2 = x_1 - x_2;
		x_1 = x_1 - x_2;
	}
	if(y_1 > y_2){
		y_1 = y_1 + y_2;
		y_2 = y_1 - y_2;
		y_1 = y_1 - y_2;
	}
	delta_x = x_2-x_1;
	delta_y = y_2-y_1;
	lenght = norm([delta_x, delta_y, 0]);
	angle = 90-atan2(delta_x, delta_y);
	x_offset = wall_thickness*sin(angle)/2;
	y_offset = -wall_thickness*cos(angle)/2;
	intersection(){
		translate([x_1,y_1,0]) {
				translate([x_offset,y_offset,0]) {
					rotate(a=angle, v=[0,0,1]){
						cube([lenght,wall_thickness,height],0);
					}
				}
			}
		cube([width,depth,height],0);
	}
}

module custom_walls(width, depth, height, wall_thickness, custom_walls){
	for(i = custom_walls) {
		x_1 = i[0][0];
		y_1 = i[0][1];
		x_2 = i[1][0];
		y_2 = i[1][1];
		wall(width, depth, height, wall_thickness, x_1, y_1, x_2, y_2);
	}
}

module grid(width, depth, height, rows, columns, wall, inner_wall){
	row_offset = (depth-2*wall)/rows;
	column_offset = (width-2*wall)/columns;
	for(i = [1:1:rows-1]){
		translate([0,(row_offset*i-inner_wall/2)+wall,0]) cube([width,inner_wall,height],0);
	}
	for(i = [1:1:columns-1]){
		translate([(column_offset*i-inner_wall/2)+wall,0,0]) cube([inner_wall,depth,height],0);
	}
}

module label_holder(width, depth, height){
	difference(){
		translate([1,-2,0]) cube([width-2, 2, height],0);
		translate([6.5,-3,2]) cube([width-13, 4, height],0);
		translate([5,-1,1]) cube([width-10, 3, height],0);
	}
}

$fn=50;

//////////ALL YOU NEED TO CHANGE/////////////				
width = 80;
depth = 80;
height = 20; 
rows = 2;
columns = 2;
floor_thickness = 0.8;
outer_wall_thickness = 1.2;
inner_wall_thickness = 0.8;
add_label_holder = true;
custom_walls = [
	[[0, 100],	[100, 0]],
	[[0, 50],	[50, 0]],
	[[50, 100],	[100, 50]]
];
/////////////////////////////////////////////

difference() {
	union() {
		main_box(width, depth, height, outer_wall_thickness, floor_thickness);
		pins(width, depth, height);
		grid(width, depth, height, rows, columns, outer_wall_thickness, inner_wall_thickness);
		custom_walls(width, depth, height, inner_wall_thickness, custom_walls);
		if(add_label_holder) label_holder(width, depth, height);
	}
	cuts(width, depth, height);
}
