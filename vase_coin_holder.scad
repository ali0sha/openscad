main_r = 35;
height = 60;
floor_thickness = 1;
radii = [23/2, 26/2, 24/2, 21.5/2]; //Rub

$fn = 200;
union(){
	linear_extrude(height){
		main2D();
	}
	cylinder(h=floor_thickness, r=main_r, center=false);
}

module cuts2D(){
	a_inc = 360/len(radii);
	for(i = [0:len(radii)-1]){
		coors = angle_to_coors(main_r - radii[i]/1, a_inc*i);
		translate([coors[0],coors[1],0]) circle(r=radii[i]);
	}
}

module base2D(){
	circle(main_r);
}

module main2D(){
	offset(r = 1) 
	offset(delta = -1)
	difference() {
		base2D();
		cuts2D();
	}
}

function angle_to_coors(r, a) = [r*cos(a), r*sin(a)];